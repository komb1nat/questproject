<?php include 'include.php';
?>
<html>
  <head>
     <link rel="stylesheet" href="style.css" type="text/css"/>
     <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
  </head>
  <body>
    <header>
    </header>
    <main>
      <h1>Обновление 1С в Екатеринбурге!</h1>
      <h2 class="update">Заполните заявку на обновление своей программы прямо сейчас!</h2>
      <p class="update">Вы получите современные версии программ 1С, содержащие последние изменения в законодательстве РФ.</p>
      <h3 class="updateHeader">Обновление 1С на выгодных условиях</h3>
      <div class="updateTable">
        <div class="updateRow">
          <div class="tableColumnName">Регулярное получение обновлений при договоре 1С:ИТС*</div><!--? maybe change tag? -->
          <p class="updateText">от  11200 р. /месяц</p>
          <p class="updateFree">+ бесплатно</p>
          <div class="updateOptions">
            <div class="optionsText">Установка актуальных обновлений</div>
            <div class="optionsText">Бесплатная линия консультаций</div>
            <div class="optionsText">12 доступных сервисов</div>
          </div>

        </div>
        <div class="updateRow">
          <div class="tableColumnName">Разовое получение обновлений</div>
          <p>Выезд программиста от 1600 р./час
            <button>Заказать</button>
          </p>
          <p>Программист удаленно от 1400 р./час
            <button>Заказать</button>
          </p>
          <p>«Блок от 4 до 8 часов»
            <span id="updateBlock">(экономия 15%)</span>
            <span id="updateBlock2">(100% предоплата)</span>
          </p>
          <p>Выезд программиста от 1400 р./час
            <button>Заказать</button>
          </p>
          <p>Программист удаленно от 1200 р./час
            <button>Заказать</button>
          </p>
        </div>

      </div>
      <div class="updatePointer">*при комплексном информационно-технологическом сопровождении пользователей программ «1С:Предприятие»</div>

      <h2 class="h2Header">Топ 10 обновляемых справочников</h2>
	  <?php
		echo '<table class="priceTable">';
		foreach ($goodsArray as $key => $value) {
			if ($value['offer'] == y){
				echo "<tr class='tableRowSpecial'><td class ='tableName'>".$value['name']."</td><td class='tablePrice'>".$value['price']."</td><td><button>".Заказать."</button>";
			}
			else{
				echo "<tr class='tableRow'><td class ='tableName'>".$value['name']."</td><td class='tablePrice'>".$value['price']."</td><td><button>".Заказать."</button>";
			}
			
		}
		echo "</table>";
	  ?>
      <hr>
      <h2 class="h2Header">Заказать расчёт стоимости обновления 1С</h2>
      <div class="orderRadio">
        <input type="radio" class="radio" name="work">Выезд программиста
        <input type="radio" class="radio" name="work">Программист удаленно
      </div>
      <div class="orderList">Интересующая услуга
        <select class="orderSelect">
          <option value="">Сопровождение 1С (услуги программистов 1С)</option>
          <option value="">Установка 1С</option>
          <option value="">Настройка 1С</option>
          <option value="">Доработка 1С</option>
          <option value="">Перенос баз, конвертация</option>
          <option value="">Поддержка 1С</option>
          <option value="">Обновление 1С</option>
          <option value="">Консультации 1С</option>
          <option value="">Помощь 1С</option>
        </select>
      </div>
      <div class="orderButton">
        <button>Рассчитать стоимость</button>
      </div>
      <hr>
      <h2 class="h2Header">Гарантии качества обновления 1С в ГК «АСП»</h2>
      <div class=textQuality>
        <li>Соответствие требованиям стандарта ISO 9001:2015</li>
        <li>Улучшение деятельности, направленной на поддержание системы управления качеством обновления программных продуктов 1С</li>
        <li>Обязательное резервное копирование баз данных</li>
        <li>Проверка обновленной системы на корректность работы</li>
        <h3 class="h3Header">В результате обновления в ГК «АСП» вы получаете:</h3>
        <li>Актуальные данные законодательства РФ</li>
        <li>Расширенный функционал системы</li>
        <li>Максимально удобный интерфейс</li>
        <li>Бесперебойную, комфортную работу программ 1С</li>
        <h3 class="textHeaderRed">Будьте уверены в сохранности своей информации! </h3>
        <h3 class="textHeaderCursive">После выполнения работ Вашу оценку проверит отдел качества. </h3>
      </div>
      <hr>
      <h2 class="h2Header">Почему обновлять 1С в ГК «АСП»?</h2>
      <div class="whyImages">
        <figure>
          <p><img src="images/expert.png" alt="" /></p>
          <figcaption>Опытные эксперты</figcaption>
        </figure>
        <figure>
          <p><img src="images/distance.png" alt="" /></p>
          <figcaption>Дистанционная форма</figcaption>
        </figure>
        <figure>
          <p><img src="images/clock.png" alt="" /></p>
          <figcaption>Оперативное обслуживание</figcaption>
        </figure>
        <figure>
          <p><img src="images/support.png" alt="" /></p>
          <figcaption>Регулярная поддержка пользователей</figcaption>
        </figure>
      </div>
      <h3 class="imagesAfter">Заказывайте обновление для автоматизации вашей работы в <span class="asp">ГК «АСП»</span> уже сейчас!</h3>
      <div class="orderButton">
        <button>Заказать</button>
      </div>

      <div class="joinText">
        <div>Вместе с нами установили обновление 1С</div>
        <div class="joinMainText">более 10 000 бухгалтеров и директоров</div>
        <div class="joinTextJoin">Присоединяйтесь!</div>
      </div>

      <hr>
      <h2 class="reviewsHeader">Отзывы пользователей 1С:ИТС</h2>
      <div class="reviews">
        <div class="review">
          <div class="reviewHeader">Lorem Ipsum</div>
          <hr>
          <div class="reviewContent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ante odio, molestie et ante id, dignissim rutrum turpis. Nullam quis.</div>
        </div>
        <div class="review">
          <div class="reviewHeader">Lorem Ipsum</div>
          <hr>
          <div class="reviewContent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ante odio, molestie et ante id, dignissim rutrum turpis. Nullam quis.</div>
        </div>
        <div class="review">
          <div class="reviewHeader">Lorem Ipsum</div>
          <hr>
          <div class="reviewContent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ante odio, molestie et ante id, dignissim rutrum turpis. Nullam quis.</div>
        </div>
      </div>
    </main>
    <footer>
    </footer>
  </body>
</html>
